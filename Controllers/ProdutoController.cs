using Microsoft.AspNetCore.Mvc;

public class ProdutoController
{
    static List<Produto> lista = new List<Produto>;

    // https://localhost:1234/produto/index
    public ActionResult Index()
    {
        Produto p1 = new Produto();
        p1.ProdutoId = 1;
        p1.Nome = "Borracha";
        p1.Preco = 2.50M;

        Produto p2 = new Produto();
        p2.ProdutoId = 2;
        p2.Nome = "Lápis";
        p2.Preco = 1.30M;
        
        if(lista.Count == 0)
        {
        lista.Add(p1);
        lista.Add(p2);
        }
        
        // /View/Produto/Index.cshtml
        return View(lista);
    }

    // https://localhost:1234/produto/create
    public ActionResult Create()
    {
        // /Views/Produto/Create.cshtml
        return View();
    }

    public ActionResult Details(int id)
    {
        Produto p;
        foreach(Produto p in lista){
            if(p.ProdutoId == id)
                return View(p);
        }

        return NotFound();
    }

}