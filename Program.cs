var builder = WebApplication.CreateBuilder(args);

// add middlewares
builder.Services.AddControllersWithViews();

var app = builder.Build();

app.MapControllerRoute("default", "/{controller=Cliente}/{action=Index}/{id?}");

app.Run();
